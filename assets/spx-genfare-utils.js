
/*
 * Extracts the base url and page context and appends path variable
 * 
 * Ex.
 * 
 * Site on -> http://localhost:1234/example
 * extract_url('/rest/service'); --> returns http://localhost:1234/example/rest/service 
 */
function extract_url(path) {		
	return [window.location.origin, '/', window.location.pathname.split('/')[1], path].join('');
}

/*
 * Log messages in javascript. can turn off all logging here by changing these values
 */
function _log() {
    try { console.log.apply(console,arguments); } catch (e) { }
}

function _err() {
    try { console.error.apply(console,arguments); } catch (e) { }
}

/*
 * use this function anywhere in JS code to return messages will return the
 * message from prop file if exists, else returns the message in square
 * brackets. use like following:
 * _getMessage("errors.wallet.already.encoded[Jason's Card][123]"); 
 * to pass in parameters
 */
function _getMessage(message) {
    var msg = message;
    var keys = new Array();
    if( message.indexOf("[") > 0 ) {
        msg = message.substr(0, message.indexOf("["));
        var count = 0;
        while( message.indexOf("[") > 0 ) {
            message = message.substr(message.indexOf("["));
            var open = message.indexOf("[");
            var close = message.indexOf("]");
            var key = message.substr(open+1,close-1);
            keys[count] = key;
            count++;
            message = message.substr(close);
        }
    }
    return $.i18n.prop(msg, keys);
}

function _searchTableSDom() {
	return '<"top">rt<"glink-search-table-footer bottom row"<"col-md-4"l><"col-md-3"i><"col-md-5"p>><"clear">';
}

function _spinnerOptions() {
    var options = {
        lines: 11, // The number of lines to draw
        length: 24, // The length of each line
        width: 14, // The line thickness
        radius: 42, // The radius of the inner circle
        scale: 1, // Scales overall size of the spinner
        corners: 1, // Corner roundness (0..1)
        color: '#000', // #rgb or #rrggbb or array of colors
        opacity: 0.25, // Opacity of the lines
        rotate: 0, // The rotation offset
        direction: 1, // 1: clockwise-1: counterclockwise
        speed: 0.8, // Rounds per second
        trail: 100, // Afterglow percentage
        fps: 20, // Frames per second when using setTimeout() as a fallback for CSS
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        className: 'spinner', // The CSS class to assign to the spinner
        top: '50%', // Top position relative to parent
        left: '50%', // Left position relative to parent
        shadow: false, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        position: 'absolute' // Element positioning
    };
    return options;
}

/*
 * Validation Registry
 */
function Validate() {
	
	var registry = {};
	
	return {
		register: function(method) {
			if(!registry[method.name]) {
				registry[method.name] = method;
			}
			return method.name;
		},
		validate: function() {
			var b = true;
			for(var v in registry) {
				b &= registry[v]();
			}
			return b;
		},
		unregister: function(method) {
			if(registry[method.name]) {
				delete registry[method.name];
			}
		}
	};
}

$(function() {
    $('.calendar').datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });
	
    $('.calendar-subscription').datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-0:+5",
        minDate: 0,
        maxDate: "+5y",
    });

   $('.calendarFuture').datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-0:+100"
   });

    var activeModule = $('#active-module');
    $('#components-navigation-toggle').click(function () {
        if (!activeModule.hasClass('open')) {
            activeModule.addClass('open');
        }
    });

    $.i18n.properties({
        name:'messages',
        path: _messagesPath,
        mode:'map',
        cache: 'true'
    });
});

var keypress_timeout = undefined;
var SearchManager = {
		
		init: function(dataTableConfig,searchUrl,transform) {
			var xhr = undefined;
			
			$('#search-results').DataTable(dataTableConfig);
			
			$('#search').keydown(function(e) {
				
				clearTimeout(keypress_timeout);
					
				var code = e.keyCode || e.which;
				if(code == 13) {
					search();
					return;
				}
				
				if($(this).val().length >= 2) {
					keypress_timeout = setTimeout(search,500);
				}
			});
			
			var loading = $('#loading').hide();
			$('#search-button').click(search);
			
			function search() {
				loading.show();
					
				//Previous search hasn't finished and we are issuing a new search, abort previous
				if(xhr && xhr.readyState != 4) {
					xhr.abort();
				}
					
				var searchResults = $('#search-results').DataTable();
				var resultsbox = $('.results-box');
					
				xhr = $.ajax({
					url: searchUrl,
					type: 'GET',
					dataType: 'json',
					data: {
						q: $('#search').val(),
						s: $('#status-filter').val() || '',
						o: $('#org-filter').val() || ''
					},
					success: function(d) {
							
						if(typeof transform === 'function') {
							d = transform(d);
						}
						else{
							console.warn("Function parameter 'transform' should be of type function");
						}
						
						resultsbox.show();
						searchResults.clear();
						searchResults.rows.add(d).draw();
						loading.hide();
					},
					error: function() {
						console.log("ajax search returned error");
						searchResults.clear();
						loading.hide();
					}
				});
			}
		}
	};

var CustomSearchManager = {
		init: function(dataTableConfig,searchUrl,transform, searchFields, dataTableSelectors, searchButtonIds) {
			var xhr = undefined;
			var buttonMap = {};
			if(dataTableSelectors){
				for(var i = 0; i < dataTableSelectors.length; i++){
					var table = $(dataTableSelectors[i]);
					var button = $('#' + searchButtonIds[i]);
					button.click(search);
					buttonMap[searchButtonIds[i]] = table.DataTable(dataTableConfig);
				}
			}else {
				$('#search-results').DataTable(dataTableConfig);
				//currently only allow auto-searching with a single results table
			}
			
			$('#search').keydown(function(e) {
				
				clearTimeout(keypress_timeout);
					
				var code = e.keyCode || e.which;
				if(code == 13) {
					search();
					return;
				}
				
				if($(this).val().length >= 2) {
					keypress_timeout = setTimeout(search,500);
				}
			});
			
			var loading = $('#loading').hide();
			
			if(!searchButtonIds){
				$('#search-button').click(search);
			}
			
			function search() {
				loading.show();
					
				//Previous search hasn't finished and we are issuing a new search, abort previous
				if(xhr && xhr.readyState != 4) {
					xhr.abort();
				}
				
				var searchResults;
				var buttonId = $(this).attr('id');
				if(buttonMap[buttonId]){
					searchResults = buttonMap[buttonId];
				} else if(dataTableSelectors){
					//we have multiple tables but search was not initiated by a linked button
					var searchButton = $('button[name="submit"]:visible');
					if(searchButton){
						buttonId = searchButton.attr('id');
						searchResults = buttonMap[buttonId];
					}
				}
				else {
					searchResults = $('#search-results').DataTable();
				}
				var resultsbox = $('.results-box');
				
				var searchFieldsData = {};
				for(i in searchFields) {
					searchFieldsData[i] = searchFields[i].val();
				}
				xhr = $.ajax({
					url: searchUrl,
					type: 'GET',
					dataType: 'json',
					data: searchFieldsData,
					success: function(d) {
							
						if(typeof transform === 'function') {
							d = transform(d);
						}
						else{
							console.warn("Function parameter 'transform' should be of type function");
						}
						
						resultsbox.show();
						searchResults.clear();
						searchResults.rows.add(d).draw();
						loading.hide();
					},
					error: function() {
						console.log("ajax search returned error");
						searchResults.clear();
						loading.hide();
					}
				});
			}
		}
	};

var DeviceSearchManager = {

		init: function(dataTableConfig,searchUrl,transform) {
			var xhr = undefined;

			$('#search-results').DataTable(dataTableConfig);

			$('#search').keydown(function(e) {

				clearTimeout(keypress_timeout);

				var code = e.keyCode || e.which;
				//Enter
				if(code == 13) {
					search();
					return;
				}

				//If searchbox contains more than two characters
				if($(this).val().length >= 2) {
					keypress_timeout = setTimeout(search,500);
				}
			});

			var loading = $('#loading').hide();
			$('#search-button').click(search);

			function search() {
				loading.show();

				//Previous search hasn't finished and we are issuing a new search, abort previous
				if(xhr && xhr.readyState != 4) {
					xhr.abort();
				}

				var searchResults = $('#search-results').DataTable();
				var resultsbox = $('.results-box');

				xhr = $.ajax({
					url: searchUrl,
					type: 'GET',
					dataType: 'json',
					data: {
						search: $('#search').val(),
						value1: $('#filter-1').val() || '',
						value2: $('#filter-2').val() || '',
						value3: $('#filter-3').val() || ''
					},
					success: function(d) {

						if(typeof transform === 'function') {
							d = transform(d);
						}
						else{
							console.warn("Function parameter 'transform' should be of type function");
						}

						resultsbox.show();
						searchResults.clear();
						searchResults.rows.add(d).draw();
						loading.hide();
					},
					error: function() {
						console.log("ajax search returned error");
						searchResults.clear();
						loading.hide();
					}
				});
			}
		}
	};

var EventTypeSearchManager = {

		init: function(dataTableConfig,searchUrl,transform) {
			var xhr = undefined;

			$('#search-results').DataTable(dataTableConfig);

			$('#search').keydown(function(e) {

				clearTimeout(keypress_timeout);

				var code = e.keyCode || e.which;
				if(code == 13) {
					search();
					return;
				}

				if($(this).val().length >= 2) {
					keypress_timeout = setTimeout(search,500);
				}
			});

			var loading = $('#loading').hide();
			$('#search-button').click(search);

			function search() {
				loading.show();

				//Previous search hasn't finished and we are issuing a new search, abort previous
				if(xhr && xhr.readyState != 4) {
					xhr.abort();
				}

				var searchResults = $('#search-results').DataTable();
				var resultsbox = $('.results-box');

				xhr = $.ajax({
					url: searchUrl,
					type: 'GET',
					dataType: 'json',
					data: {
						search: $('#search').val(),
						severity: $('#filter-Severity').val() || '',
						devicetype: $('#filter-DeviceType').val() || '',
						category: $('#filter-Category').val() || '',
						status: $('#filter-Status').val() || ''
					},
					success: function(d) {

						if(typeof transform === 'function') {
							d = transform(d);
						}
						else{
							console.warn("Function parameter 'transform' should be of type function");
						}

						resultsbox.show();
						searchResults.clear();
						searchResults.rows.add(d).draw();
						loading.hide();
					},
					error: function() {
						console.log("ajax search returned error");
						searchResults.clear();
						loading.hide();
					}
				});
			}
		}
	};

var RealtimeSearchManager = {

	init: function(dataTableConfig,searchUrl,searchData,transform,errorHandler,clickInputs,changeInputs,textInputs) {
		$('#search-results').DataTable(dataTableConfig);
		this.searchUrl = searchUrl;
		this.loading = $('#loading').hide();
		this.searchData = searchData;
		this.transform = transform;
		this.errorHandler = errorHandler;
		
		$.each(clickInputs, function(index, value) {
			$(value).click(RealtimeSearchManager.search);
		});
		$.each(changeInputs, function(index, value) {
			$(value).change(RealtimeSearchManager.search);
		});
	},
	searchUrl: null,
	searchData: null,
	transform: null,
	loading: null,
	xhr: null,
	errorHandler: null,
	search: function() {
		//Previous search hasn't finished and we are issuing a new search, abort previous
		if(this.xhr && this.xhr.readyState != 4) {
			this.xhr.abort();
		}

		var searchResults = $('#search-results').DataTable();
		var resultsbox = $('.results-box');
		this.xhr = $.ajax({
			url: RealtimeSearchManager.searchUrl,
			type: 'GET',
			dataType: 'json',
			data: RealtimeSearchManager.searchData(),
			success: function(d) {

				if(typeof RealtimeSearchManager.transform === 'function') {
					d = RealtimeSearchManager.transform(d);
				}
				else{
					console.warn("Function parameter 'transform' should be of type function");
				}

				resultsbox.show();
				searchResults.clear();
				searchResults.rows.add(d).draw();
				RealtimeSearchManager.loading.hide();
			},
			error: function() {
				RealtimeSearchManager.errorHandler("ajax search returned error","realtime.search.failure");
				RealtimeSearchManager.loading.hide();
			}
		});
	}
};
function parseTime(time) {
    var temp = time.split(' ')[0].split('/');
    return [temp[2]+'-'+temp[0]+'-'+temp[1]];
}
