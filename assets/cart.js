$(document).ready(function(){												
        $('.update-total').on('click', function(e) {
        	$(".alert").hide();
        	e.preventDefault();
        	var quantities = [];
        	var quantityFields = $('.quantity');
        	for (var i = 0; i < quantityFields.length; i++) {
        		var qtyField = quantityFields.eq(i);
        		var qtyValue = qtyField.val();
        		var quantityObj = {};
        		var clamped_qty = qtyValue < 1 ? 1 : qtyValue;
        		qtyField.val(clamped_qty);
        		quantityObj['quantity'] = clamped_qty.toString();
        		quantityObj['orderItem'] = qtyField.data('order-item').toString();
        		quantities.push(quantityObj);
        	}
        	$.ajax({
				url: extract_url('/store/updateTotal'),
				type: 'POST',
	            data : {
	            	_csrf : document.getElementsByName('_csrf')[0].value,
	            	quantities : JSON.stringify(quantities)
	            },
				success : function(data) {
		            if (data.status == "error") {
		                // Revert quantity field values back to last values
		                for (var j = 0; j < quantityFields.length; j++) {
		                    var qtyField = quantityFields.eq(j);
		                    qtyField.val(qtyField.data('previous'));
		                }
		                addUpdateErrorContainer();
		            	$('#updateTotalErrMsg').text(data.value);
		            	$('#updateTotalError').removeClass("hidden").show();
		                return;
		            }
		            // Update quantity fields to track current quantity values
		            for (var j = 0; j < quantityFields.length; j++) {
		                var qtyField = quantityFields.eq(j);
		                qtyField.data('previous', qtyField.val());
		            }
		            $('#amountOrder').text(data.value);
				},
		        error : function(data) {
		        	addUpdateErrorContainer();

					if (data.status === 200) {
						location.reload();
					} else {
						// Revert quantity field values back to last values
						for (var j = 0; j < quantityFields.length; j++) {
							var qtyField = quantityFields.eq(j);
							qtyField.val(qtyField.data('previous'));
						}

						$('#updateTotalErrMsg').text(_getMessage('validator.cart.update.error'));
						$('#updateTotalError').removeClass("hidden").show();
					}
		        },
	            dataType : "json"
			});
      });
});

function addUpdateErrorContainer() {
    if ($('#updateTotalError').length < 1) {
        var html = '<div id="updateTotalError" class="alert alert-dismissable alert-danger hidden">';
        html += '<button type="button" class="close" data-dismiss="alert">×</button>';
        html += '<ul><li>';
        html += '<span id="updateTotalErrMsg"></span>';
        html += '</li></ul></div>';
        $('.tickets').eq(0).before(html);
    }
}
