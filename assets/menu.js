$(document).ready(function(){												

       //Navigation Menu Slider
        $('#nav-expander').on('click',function(e){
      		e.preventDefault();
      		$('body').toggleClass('nav-expanded');
      	});
      	$('#nav-close').on('click',function(e){
      		e.preventDefault();
      		$('body').removeClass('nav-expanded');
      	});
      });
	  
	  $('.nav-tabs-dropdown').on('click', 'li:not(\'.active\') a', function (event) {
    $(this).closest('ul').removeClass('open');
}).on('click', 'li.active a', function (event) {
    $(this).closest('ul').toggleClass('open');
});